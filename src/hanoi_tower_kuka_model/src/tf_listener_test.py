#!/usr/bin/python
import rospy
from std_msgs.msg import String
from gazebo_msgs.msg import ModelStates
import tf
import numpy as np
from geometry_msgs.msg import PoseStamped, Pose, Point, Quaternion
from std_msgs.msg import Header
from math import pi as pi_number
origin, xaxis, yaxis, zaxis = (0, 0, 0), (1, 0, 0), (0, 1, 0), (0, 0, 1)

def PoseStamped_2_mat(p):
    #Get Transformation matrix from pose and quartenions
    q = p.pose.orientation
    pos = p.pose.position
    T = tf.transformations.quaternion_matrix([q.x,q.y,q.z,q.w])
    #adding positions to the positon part of the transformation matrix
    T[:3,3] = np.array([pos.x,pos.y,pos.z])
    return T

if __name__ == "__main__":
    print('test')
    rospy.init_node('hanoi_tower_reference_frame_listener')
    rospy.sleep(5)
    print("After sleep")
    listener = tf.TransformListener()
    listener.waitForTransform("/world", "/iiwa_link_ee", rospy.Time(), rospy.Duration(4.0))
    rate = rospy.Rate(1)
    while not rospy.is_shutdown():
        try:
            (robot_location,rot) = listener.lookupTransform('/world','/iiwa_link_ee', rospy.Time(0))
            print("End effector location with respect to world frame",robot_location)
            (robot_location_end_effector_frame,rot_end_effector_frame) = listener.lookupTransform('/world','/end_effector_frame', rospy.Time(0))
            print("End effector location with respect to world",robot_location_end_effector_frame)            
            (robot_location,rot) = listener.lookupTransform('/iiwa_link_ee','/end_effector_frame', rospy.Time(0))
            print("End effector location with respect to iiwa_link_ee",robot_location)
            (robot_location_d300,rot_d300) = listener.lookupTransform('/world','/hanoi_tower_d300_model1_frame', rospy.Time(0))
            print("Hanoo Tower d300 with respect to world frame",robot_location_d300)
            (robot_location,rot) = listener.lookupTransform('/world','/hanoi_tower_d400_model1_frame', rospy.Time(0))
            print("Hanoo Tower d400 with respect to world frame",robot_location)
            (robot_location,rot) = listener.lookupTransform('/world','/hanoi_tower_d500_model1_frame', rospy.Time(0))
            print("Hanoo Tower d500 with respect to world frame",robot_location)
            (robot_location,rot) = listener.lookupTransform('/world','/hanoi_tower_d600_model1_frame', rospy.Time(0))
            print("Hanoo Tower d600 with respect to world frame",robot_location)
            (robot_location,rot) = listener.lookupTransform('/world','/hanoi_tower_d700_model1_frame', rospy.Time(0))
            print("Hanoo Tower d700 with respect to world frame",robot_location)

            (robot_location,rot) = listener.lookupTransform('/world','/hanoi_board_middle_below_reference_frame', rospy.Time(0))
            print("hanoi_board_middle_below_reference_frame with respect to world frame",robot_location)
            (robot_location,rot) = listener.lookupTransform('/world','/hanoi_board_middle_top_reference_frame', rospy.Time(0))
            print("hanoi_board_middle_top_reference_frame with respect to world frame",robot_location)

            (robot_location,rot) = listener.lookupTransform('/world','/hanoi_board_right_top_reference_frame', rospy.Time(0))
            print("hanoi_board_right_top_reference_frame with respect to world frame",robot_location)
            (robot_location,rot) = listener.lookupTransform('/world','/hanoi_board_left_top_reference_frame', rospy.Time(0))
            print("hanoi_board_left_top_reference_frame with respect to world frame",robot_location)
            print(rot)
            (roll, pitch, yaw) = tf.transformations.euler_from_quaternion (rot_d300)
            print("Euler_list in radian: ",roll,pitch,yaw)


            p_end_effector = PoseStamped(header = Header(frame_id="End_Effector_Transformation_Matrix"),pose=Pose(position=Point(*robot_location_end_effector_frame),orientation=Quaternion(*rot_end_effector_frame)))
            print("End Effector Matrix",PoseStamped_2_mat(p_end_effector))

            p_token_d300 = PoseStamped(header = Header(frame_id="d300_Transformation_Matrix"),pose=Pose(position=Point(*robot_location_d300),orientation=Quaternion(*rot_d300)))
            print("End Effector Token d300",PoseStamped_2_mat(p_token_d300))

            rotation_matrix_y = tf.transformations.rotation_matrix(pi_number,yaxis)
            p_token_d300_rotated_by_y_axis = np.matmul(rotation_matrix_y,PoseStamped_2_mat(p_token_d300))
            print("KKK",p_token_d300_rotated_by_y_axis)

            rotated_token_d300_quartenion=tf.transformations.quaternion_from_matrix(p_token_d300_rotated_by_y_axis)
            print("quar",rotated_token_d300_quartenion)

            print(robot_location.x)
            rate.sleep()
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue
    
    rospy.spin()