/*
 * Copyright (C) 2012 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include <gazebo/transport/transport.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/gazebo_client.hh>
#include <iostream>
#include "ros/ros.h"
#include <geometry_msgs/Wrench.h>
/////////////////////////////////////////////////
// Function is called everytime a message is received.

geometry_msgs::Wrench FT;
void cb(ConstWrenchStampedPtr &msg2)
{
  // Dump the message contents to stdout.
  std::cout << "Force and Torque Values: " << std::endl;
  std::cout << msg2->wrench().force().x()<< std::endl;
  std::cout << msg2->wrench().force().y()<< std::endl;
  std::cout << msg2->wrench().force().z()<< std::endl;
  std::cout << msg2->wrench().torque().x()<< std::endl;
  std::cout << msg2->wrench().torque().y()<< std::endl;
  std::cout << msg2->wrench().torque().z()<< std::endl;

  FT.force.x = msg2->wrench().force().x();
  FT.force.y = msg2->wrench().force().y();
  FT.force.z = msg2->wrench().force().z();

  FT.torque.x = msg2->wrench().torque().x();
  FT.torque.y = msg2->wrench().torque().y();
  FT.torque.z = msg2->wrench().torque().z();


}

/////////////////////////////////////////////////
int main(int _argc, char **_argv)
{
  // Load gazebo
  gazebo::client::setup(_argc, _argv);
  ros::init(_argc, _argv, "gazebo_physic_analyser");
  ros::NodeHandle n;
  ros::Publisher end_effector_forces = n.advertise<geometry_msgs::Wrench>("/iiwa/end_effector_force_publisher", 1000);

  // Create our node for communication
  gazebo::transport::NodePtr node(new gazebo::transport::Node());
  node->Init();

  // Listen to Gazebo world_stats topic
  gazebo::transport::SubscriberPtr sub = node->Subscribe("~/iiwa14/iiwa_joint_7/force_torque/wrench", cb);

  // Busy wait loop...replace with your own code as needed.
  while (true){
    gazebo::common::Time::MSleep(1);
    end_effector_forces.publish(FT);



    }

  // Make sure to shut everything down.
  gazebo::client::shutdown();
}
