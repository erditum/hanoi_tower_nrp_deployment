#include <ros/ros.h>
#include "std_msgs/String.h"
#include <geometry_msgs/Wrench.h>
#include <sensor_msgs/JointState.h>
#include <iostream>
#include <string>
#include <vector> 
#include <stdio.h>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/chaindynparam.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <Eigen/Dense>
using namespace std;

// 6x1 vector for end effector force and torques values 
Eigen::VectorXd end_effector_forces(6);

Eigen::VectorXd joint_states_position(9);
Eigen::VectorXd joint_states_velocity(9);
Eigen::VectorXd joint_states_effort(9);


void iiwa_joint_states(const sensor_msgs::JointState &joint_states){

  // This function will print the position, velocity, and effort values of the joints.
  // 7  revolute joints  and +2 prismatic gripper joints
  for (int i=0 ; i<9 ; i++){

  // std::cout << "Position: "<< joint_states.name[i] << ":   " << joint_states.position[i] << std::endl;
  // std::cout << "Velocity: "<< joint_states.name[i] << ":   " << joint_states.velocity[i] << std::endl;
  // std::cout << "Effort: "<< joint_states.name[i] << ":   " << joint_states.effort[i] << std::endl;
    joint_states_position(i) = joint_states.position[i];
    joint_states_velocity(i) = joint_states.velocity[i];
    joint_states_effort(i) = joint_states.effort[i];
    
  }
}

void end_effector_force_torque(const geometry_msgs::Wrench &FT)

{
  // Print message for end_effector force and torque values from the subscriber 
  end_effector_forces(0) = FT.force.x;
	end_effector_forces(1) = FT.force.y;
	end_effector_forces(2) = FT.force.z;
	end_effector_forces(3) = FT.torque.x;
	end_effector_forces(4) = FT.torque.y;
	end_effector_forces(5) = FT.torque.z;
  // std::cout <<  end_effector_forces << std::endl;


}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "robot_dynamics_calculator");
  KDL::Tree my_tree;
  string robot_desc_string;
  ros::AsyncSpinner spinner(1);
  ros::NodeHandle node_handle;
  spinner.start();

  ros::Publisher chatter_pub = node_handle.advertise<std_msgs::String>("Jacobian_matrix", 10);
  ros::Subscriber sub = node_handle.subscribe("end_effector_force_publisher", 10, end_effector_force_torque);
  ros::Subscriber iiwa_joint_state_sub = node_handle.subscribe("joint_states", 10, iiwa_joint_states);

  double ros_frequency = 30;
  ros::Rate loop_rate(ros_frequency); // 10Hz ==> 0.1 seconds

  node_handle.param("/iiwa/robot_description", robot_desc_string, string());
  if (!kdl_parser::treeFromString(robot_desc_string, my_tree)){
        ROS_ERROR("Failed to construct kdl tree");
        return false;
    }
    KDL::Chain kuka_arm_chain;
    my_tree.getChain("iiwa_link_0", "iiwa_link_ee", kuka_arm_chain);




    int number_of_joints =  kuka_arm_chain.getNrOfJoints();
    ROS_WARN("Number of joints: %d\n", number_of_joints);

    KDL::ChainJntToJacSolver jacobsolver(kuka_arm_chain);
    KDL::Jacobian jacobian_matrix(number_of_joints);
    KDL::JntArray q_in(number_of_joints);
    KDL::JntArray q_in_dot(number_of_joints);

    // Initialize the mass matrix
    KDL::Vector g(0, 0, -9.8);
    KDL::ChainDynParam dyn_params(kuka_arm_chain, g);
    KDL::JntSpaceInertiaMatrix M(number_of_joints);


    // Initialize the Coriolis matrix
    KDL::JntArray G(number_of_joints);
    Eigen::VectorXd Coriolis_Matrix(number_of_joints);
    
    // Initialize the gravity matrix
    KDL::JntArray gra(number_of_joints);
    Eigen::VectorXd Gravity_Matrix(number_of_joints);



   
  Eigen::VectorXd joint_states_position_time_t(7);
  Eigen::VectorXd joint_states_position_time_t_1(7);
  Eigen::VectorXd joint_states_position_time_t_2(7);
  Eigen::VectorXd calculated_joint_velocity(7);
  Eigen::VectorXd calculated_joint_acceleration(7);

  ros::Time now = ros::Time::now();
  ros::Time now_before = ros::Time::now();
  ros::Time now_before_before = ros::Time::now();
  while (ros::ok())
  {

    // joint_states_position.segment(2,7) => joint position values for joints 1 -- 7 (except gripper)
    // std::cout << "Joint positon: " << std::endl;
    // std::cout << joint_states_position.segment(2,7) << std::endl;
    // std::cout << "Joint velocity: " << std::endl;
    // std::cout << joint_states_velocity.segment(2,7) << std::endl;

    // std::cout << joint_states_velocity.segment(2,7) << std::endl;
    // std::cout << "-------" << std::endl;

    // joint_states_position_now  = joint_states_position.segment(2,7);


    // calculated_joint_velocity = (joint_states_position_now - joint_states_position_old ) / 0.1;
    // std::cout << "Calculated vel" << std::endl;
    // std::cout <<  calculated_joint_velocity << std::endl;
    // std::cout << "-------" << std::endl;


    for (int i = 0; i < 3 ; i++)
    {
    
    if ((i % 3) == 0)
    {
      now_before_before = ros::Time::now();
      joint_states_position_time_t_2 = joint_states_position.segment(2,7);
    }

    else if ((i % 3 ) == 1)
    {
      now_before = ros::Time::now();
      joint_states_position_time_t_1 = joint_states_position.segment(2,7);
    }
    else
    {
      now = ros::Time::now();
      joint_states_position_time_t = joint_states_position.segment(2,7);
    }
    loop_rate.sleep();
    }

    std::cout << now_before_before << std::endl;
    std::cout << now_before << std::endl;
    std::cout << now << std::endl;

    std::cout << "Position values" << std::endl;
    std::cout << joint_states_position_time_t << std::endl;
    std::cout << "------------" << std::endl;


    calculated_joint_velocity = (joint_states_position_time_t - joint_states_position_time_t_1) / (1.0/(double)ros_frequency);
    calculated_joint_acceleration = (joint_states_position_time_t - 2*joint_states_position_time_t_1 + joint_states_position_time_t_2) / (1.0/(double)ros_frequency);

    std::cout << "Velocity values" << std::endl;
    std::cout << calculated_joint_velocity << std::endl;
    std::cout << "------------" << std::endl;

    std::cout << "Acceleraton values" << std::endl;
    std::cout << calculated_joint_acceleration  << std::endl;
    std::cout << "------------" << std::endl;


    for (int i = 0 ; i<number_of_joints ; i++){
                // first 2 variables of the joint_states_position is related to gripper position.
      q_in(i) = joint_states_position_time_t(i);
      q_in_dot(i) = calculated_joint_velocity(i);
    }

    dyn_params.JntToMass(q_in, M);
    Eigen::MatrixXd Mass_Matrix(number_of_joints,number_of_joints);
    for(int i=0;i<number_of_joints;i++) {
        for(int j=0;j<number_of_joints;j++)
            { 
            // std::printf("%f, ", M(i,j));
            // std::printf("\n");
            Mass_Matrix(i,j)= M(i,j);
            }
    }

    std::cout << "Mass Matrix: " << std::endl;
    std::cout << Mass_Matrix << std::endl;
    std::cout << "------------" << std::endl;

    dyn_params.JntToCoriolis(q_in,q_in_dot, G);

    for(int i=0;i<number_of_joints;i++) {
            Coriolis_Matrix(i) = G(i);

    }

    std::cout << "Coriolis Matrix: " << std::endl;
    std::cout << Coriolis_Matrix << std::endl;
    std::cout << "------------" << std::endl;

    dyn_params.JntToGravity(q_in,gra);

    for(int i=0;i<number_of_joints;i++) {
            Gravity_Matrix(i) = gra(i);
    }

    std::cout << "Gravity_Matrix: " << std::endl;
    std::cout << Gravity_Matrix << std::endl;
    std::cout << "------------" << std::endl;


    jacobsolver.JntToJac(q_in,jacobian_matrix);

    std::cout << "Jacobian Matrix" << std::endl;

    Eigen::MatrixXd jacb_matr;
    jacb_matr = jacobian_matrix.data;
    std::cout << jacb_matr << std::endl;
    std::cout << "------------" << std::endl;


    std::cout << "Jacobian Matrix Transpose" << std::endl;
    std::cout << jacb_matr.transpose() << std::endl;
    std::cout << "------------" << std::endl;


    std::cout << "Measured end effector forces" << std::endl;
    std::cout << end_effector_forces << std::endl;
    std::cout << "------------" << std::endl;
    // Torque Calculations
    std::cout << "Required Torque Value for Joints" << std::endl;
    std::cout << Mass_Matrix * calculated_joint_acceleration + Coriolis_Matrix + Gravity_Matrix + jacb_matr.transpose() * end_effector_forces << std::endl;

    ros::spinOnce();
    // loop_rate.sleep();
  }
 

  // END_TUTORIAL
  return 0;
}