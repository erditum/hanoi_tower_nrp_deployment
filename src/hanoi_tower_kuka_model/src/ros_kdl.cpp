#include <stdio.h>
#include <ros/ros.h>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/chaindynparam.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <Eigen/Dense>
using namespace std;


double gen_rand(double min, double max)
{
  int rand_num = rand()%100+1;
  double result = min + (double)((max-min)*rand_num)/101.0;
  return result;
}


int main(int argc, char **argv) {
    ros::init(argc, argv, "test_inertia");
    KDL::Tree my_tree;
    ros::NodeHandle node;
    string robot_desc_string;
    node.param("/iiwa/robot_description", robot_desc_string, string());
    if (!kdl_parser::treeFromString(robot_desc_string, my_tree)){
        ROS_ERROR("Failed to construct kdl tree");
        return false;
    }
    KDL::Chain kuka_arm_chain;
    my_tree.getChain("iiwa_link_0", "iiwa_link_ee", kuka_arm_chain);

    int number_of_joints =  kuka_arm_chain.getNrOfJoints();
    ROS_WARN("Number of joints: %d\n", number_of_joints);

    KDL::Vector g(0, 0, -9.8);
    KDL::ChainDynParam dyn_params(kuka_arm_chain, g);
    KDL::JntArray q(number_of_joints);
    KDL::JntArray q_dot(number_of_joints);
    q(0) = 0.14;
    q(1) = 0.1;
    q(2) = 0.1;
    q(3) = 0.2;
    q(4) = 0.3;
    q(5) = 0.4;
    q(6) = 0.1;
    q_dot(1) = 0.2;

    ROS_WARN("%f\n", q.data[0]);
    ROS_WARN("%f\n", q.data[1]);
    ROS_WARN("%f\n", q.data[2]);
    ROS_WARN("%f\n", q.data[3]);
    KDL::JntSpaceInertiaMatrix M(number_of_joints);
    dyn_params.JntToMass(q, M);
    Eigen::MatrixXd Mass_Matrix(number_of_joints,number_of_joints);
    ROS_WARN("%d %d\n", M.columns(), M.rows());
    for(int i=0;i<number_of_joints;i++) {
        for(int j=0;j<number_of_joints;j++)
            { 
            // std::printf("%f, ", M(i,j));
            // std::printf("\n");
            Mass_Matrix(i,j)= M(i,j);
            }
    }

    std::cout << "Mass Matrix: " << std::endl;
    std::cout << Mass_Matrix << std::endl;



    KDL::JntArray G(number_of_joints);
    Eigen::VectorXd Coriolis_Matrix(number_of_joints);
    dyn_params.JntToCoriolis(q,q_dot, G);

     ROS_WARN("%d %d\n", G.columns(), G.rows());
    for(int i=0;i<number_of_joints;i++) {
            // std::printf("%f, ", G(i));
            Coriolis_Matrix(i) = G(i);

    }

    std::cout << "Coriolis Matrix: " << std::endl;
    std::cout << Coriolis_Matrix << std::endl;


    KDL::JntArray gra(number_of_joints);
    Eigen::VectorXd Gravity_Matrix(number_of_joints);
    dyn_params.JntToGravity(q,gra);

     ROS_WARN("%d %d\n", gra.columns(), gra.rows());
    for(int i=0;i<number_of_joints;i++) {
            Gravity_Matrix(i) = gra(i);
    }

    std::cout << "Gravity_Matrix: " << std::endl;
    std::cout << Gravity_Matrix << std::endl;




    KDL::ChainFkSolverPos_recursive fksolver(kuka_arm_chain); 
    KDL::Frame T;
    fksolver.JntToCart(q,T); 
    
    std::cout << "" << std::endl;
    std::cout << "Transformation Matrix: " << std::endl;
    
    Eigen::MatrixXd Frame_Matrix(4,4);
    Frame_Matrix << T(0,0),T(0,1),T(0,2),T(0,3),
                    T(1,0),T(1,1),T(1,2),T(1,3),
                    T(2,0),T(2,1),T(2,2),T(2,3),
                    T(3,0),T(3,1),T(3,2),T(3,3);

    std::cout << Frame_Matrix << std::endl;

    KDL::ChainJntToJacSolver jacobsolver(kuka_arm_chain);
    KDL::Jacobian jacobian_matrix(number_of_joints);
    KDL::JntArray q_in(number_of_joints);
    for(int j=0; j < number_of_joints; j++)
   {
     q_in(j) = gen_rand(-M_PI,M_PI);
   }


   q_in(0) = 0.427513;
   q_in(1) = 0.900696;
   q_in(2) = -2.04115;
   q_in(3) = -0.570877;
   q_in(4) = 0.632102;
   q_in(5) = -0.587905;
   q_in(6) = 1.90345;


    jacobsolver.JntToJac(q_in,jacobian_matrix);

    std::cout << "Jacobian Matrix" << std::endl;
    std::cout<< jacobian_matrix.columns() << std::endl;
    std::cout<< jacobian_matrix.rows() << std::endl;
    Eigen::MatrixXd jacb_matr;
    jacb_matr = jacobian_matrix.data;
    std::cout << jacb_matr << std::endl;


//    References
/* 
https://github.com/CFlyingDragon/Multi_arm_robot/tree/master
https://github.com/CFlyingDragon/Multi_arm_robot/blob/84753122d0376c996284f6d252330eb5bb70867a/catkin_ws/src/robot_kinematic/kdl_kinematic/demo/kukaLWRtestHCG.cpp
https://github.com/CFlyingDragon/Multi_arm_robot/blob/84753122d0376c996284f6d252330eb5bb70867a/catkin_ws/src/robot_kinematic/kdl_kinematic/lib/kdl_robot_base.cpp
 */


}