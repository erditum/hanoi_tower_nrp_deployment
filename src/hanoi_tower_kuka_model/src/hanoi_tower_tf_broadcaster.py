#!/usr/bin/python
import rospy
from std_msgs.msg import String
from gazebo_msgs.msg import ModelStates
import tf
import numpy as np
from math import pi
#  This matchers is used to find how many ea_robots there are in the Gazebo
obj_starts_with = "hanoi_tower"
hanoi_board_pilar_height = 0.105
hanoi_board_pilar_distance = 0.1 * 1.2
gripper_length = 0.1
def shutdownhook():
    print('System is being closed')

def callback(data):
    object_list_in_gazebo = data.name #All available objects name in Gazebo
    all_objects_startswith = [i for i in data.name if i.startswith(obj_starts_with)] # All objects in Gazebo starts with specified string
    index_number = object_list_in_gazebo.index('hanoi_board')
    trans_board = data.pose[index_number].position
    orien_board = data.pose[index_number].orientation

    #Get the robot position with respect to gazebo
    index_number_robot = object_list_in_gazebo.index('iiwa14')
    trans_robot = data.pose[index_number_robot].position
    orien_robot = data.pose[index_number_robot].orientation


    #This rotation is applied to allign these frames with the token frame
    # T = tf.transformations.quaternion_matrix([orien.x,orien.y,orien.z,orien.w])
    # rotation_matrix = tf.transformations.rotation_matrix(-pi/2,(0,0,1)) #rotate around z axis
    # new_frame_orientation = np.matmul(T,rotation_matrix)
    # orien_q = tf.transformations.quaternion_from_matrix(new_frame_orientation)
    br1 = tf.TransformBroadcaster()
    br1.sendTransform((-trans_robot.x, -trans_robot.y, -trans_robot.z),
                         (0, 0, 0, 1),
                         rospy.Time.now(),
                         'gazebo_reference',
                         "world")

    br_hanoi_board_middle = tf.TransformBroadcaster()
    br_hanoi_board_middle.sendTransform((trans_board.x, trans_board.y, trans_board.z),
                            (orien_board.x,orien_board.y,orien_board.z,orien_board.w),
                            rospy.Time.now(),
                            'hanoi_board_middle_below_reference_frame',
                            "gazebo_reference")
    br_hanoi_board_middle = tf.TransformBroadcaster()

    br_hanoi_board_middle.sendTransform((trans_board.x, trans_board.y, trans_board.z+hanoi_board_pilar_height),
                             (orien_board.x,orien_board.y,orien_board.z,orien_board.w),
                            rospy.Time.now(),
                            'hanoi_board_middle_top_reference_frame',
                            "gazebo_reference")
    br_hanoi_board_left = tf.TransformBroadcaster()

    br_hanoi_board_left.sendTransform((trans_board.x, trans_board.y-hanoi_board_pilar_distance, trans_board.z+hanoi_board_pilar_height),
                             (orien_board.x,orien_board.y,orien_board.z,orien_board.w),
                            rospy.Time.now(),
                            'hanoi_board_left_top_reference_frame',
                            "gazebo_reference")
    br_hanoi_board_right = tf.TransformBroadcaster()

    br_hanoi_board_right.sendTransform((trans_board.x, trans_board.y+hanoi_board_pilar_distance, trans_board.z+hanoi_board_pilar_height),
                             (orien_board.x,orien_board.y,orien_board.z,orien_board.w),
                            rospy.Time.now(),
                            'hanoi_board_right_top_reference_frame',
                            "gazebo_reference")
    # number_of_robots = len(matching)
    # #print("There is {} robots in Gazebo".format(number_of_robots))

    # for current_robot in matching:

    #     index_number = object_list_in_gazebo.index(current_robot)
        #print("Position of " + current_robot + ": ", data.pose[index_number].position)
    br = tf.TransformBroadcaster()
    br.sendTransform((0,0,gripper_length),
                         (0,0,0,1),
                         rospy.Time.now(),
                         'end_effector_frame',
                         "iiwa_link_ee")




    for each_obj in all_objects_startswith:
        index_number = object_list_in_gazebo.index(each_obj)
        trans = data.pose[index_number].position
        orien = data.pose[index_number].orientation
        br_obj = tf.TransformBroadcaster()
        br_obj.sendTransform((trans.x, trans.y, trans.z),
                            (orien.x, orien.y, orien.z, orien.w),
                            rospy.Time.now(),
                            each_obj+str('_frame'),
                            "gazebo_reference")


if __name__ == "__main__":
    rospy.init_node('hanoi_tower_reference_frame_broadcaster')
    rospy.logwarn("ROS TF Broadcaster is initialized...")
    rospy.Subscriber("/gazebo/model_states", ModelStates, callback)
    rospy.Rate(10)
    rospy.on_shutdown(shutdownhook)
    rospy.spin()
